![Tesser_logo](https://docs.google.com/drawings/d/e/2PACX-1vT26NziYMaLrGHlvNspiJ9dOjXR6hZyVxrGwfkgV1MwvMTWtAWh5ZUqMsit5gSllXemcGajjddqfqnc/pub?w=131&h=129)

The [**TesserAkt** library](https://bitbucket.org/AdrianArtacho/tesserakt/src/master/) is a collection of MaxForLive devices designed for real-time midi manipulation. These devices were developed in the context of the Fraktale Lab, within the artistic research project [Atlas of Smooth Spaces](https://www.the-smooth.space/) ([FWF 640](https://pf.fwf.ac.at/de/wissenschaft-konkret/project-finder/51560)) at the University of Music and Performing Arts Vienna.

Dieses Werk wurde mit freundlicher Unterstützung des österreichischen Bundesministeriums für Kunst, Kultur, öffentlicher Dienst und Sport realisiert.

![BMKÖES](https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/BMKOES_AT_Logo.svg/640px-BMKOES_AT_Logo.svg.png)

[www.bmkoes.gv.at](https://www.bmkoes.gv.at/)


### Credits and access

This environement is developed and mantained by [Adrian Artacho](https://bitbucket.org/AdrianArtacho/). The online repository can be found here:

[https://bitbucket.org/AdrianArtacho/tesserakt/](https://bitbucket.org/AdrianArtacho/tesserakt/src/master/)

---

| Name                                                                          | Description                                           |
| ----------------------------------------------------------------------------- | ----------------------------------------------------- |
| [Tesser_cmd](https://bitbucket.org/AdrianArtacho/tesser_cmd/)                 | Launches a function based on midi input               |
| [tesser_automidi](https://bitbucket.org/AdrianArtacho/tesser_automidi/)       | Similar to 'autotune', reshapes a midi input          |
| [tesser_block](https://bitbucket.org/AdrianArtacho/tesser_block/)             | Blocks CC/Midinotes dynamically                       |
| [tesser_buffer](https://bitbucket.org/AdrianArtacho/tesser_buffer/)           | This device allows to save and recall bits of audio   |
| [tesser_cc2note](https://bitbucket.org/AdrianArtacho/tesser_cc2note/)         | Converts a midi inout onto midinotes                  |
| [tesser_cc2params](https://bitbucket.org/AdrianArtacho/tesser_cc2params/)     | Maps CC input onto ranges and parameters              |
| [tesser_cc2signal](https://bitbucket.org/AdrianArtacho/tesser_cc2signal/)     | Create a signal-structured stream of midi             |
| [tesser_chains](https://bitbucket.org/AdrianArtacho/tesser_chains/)           | Renames midinotes based on lists                      |
| [tesser_clip2cc](https://bitbucket.org/AdrianArtacho/tesser_clip2cc/)         | Translates midinotes to CC values                     |
| [tesser_clips](https://bitbucket.org/AdrianArtacho/tesser_clips/)             | Launches clips via midinotes or CC                    |
| [tesser_cue](https://bitbucket.org/AdrianArtacho/tesser_cue/)                 | Aural warnings to the performer                       |
| [tesser_delay](https://bitbucket.org/AdrianArtacho/tesser_delay/)             | Takes a midi input and delays it by an amount of time |
| [tesser_dynamic](https://bitbucket.org/AdrianArtacho/tesser_dynamic/)         | Manipulate note velocity in different ways            |
| [tesser_fade](https://bitbucket.org/AdrianArtacho/tesser_fade/)               | Fades in/out (increases/reduces midi velocities)      |
| [tesser_fractal](https://bitbucket.org/AdrianArtacho/tesser_fractal/)         | Fractal Video manipulation                            |
| [tesser_function](https://bitbucket.org/AdrianArtacho/tesser_function/)       | Manipulates midi input based on a function            |
| [tesser_funnel](https://bitbucket.org/AdrianArtacho/tesser_funnel/)           | Maps differently sized lists of midi IN/OUT values    |
| [tesser_gate](https://bitbucket.org/AdrianArtacho/tesser_gate/)               | Open/Close the midi stream dynamically                |
| [tesser_gesture](https://bitbucket.org/AdrianArtacho/tesser_gesture/)         | Extract a gesture from a stream of midi values        |
| [tesser_inscore](https://bitbucket.org/AdrianArtacho/tesser_inscore/)         | Interfaces with INScore (midi input)                  |
| [tesser_livescore](https://bitbucket.org/AdrianArtacho/tesser_livescore/)     | Score display of midi                                 |
| [tesser_mirror](https://bitbucket.org/AdrianArtacho/tesser_mirror/)           | Mirrors midi values dynamically based on a 'center'   |
| [tesser_mutate](https://bitbucket.org/AdrianArtacho/tesser_mutate/)           | Introduces mutation in a given midi sequence          |
| [tesser_note2cc](https://bitbucket.org/AdrianArtacho/tesser_note2cc/)         | Converts midinotes onto CC                            |
| [tesser_pedal](https://bitbucket.org/AdrianArtacho/tesser_pedal/)             | Specific midi keyboard pedal interface                |
| [tesser_pgch](https://bitbucket.org/AdrianArtacho/tesser_pgch/)               | Generate program change messages based on midi        |
| [tesser_ramp](https://bitbucket.org/AdrianArtacho/tesser_ramp/)               | Generates ramp of values for a given time             |
| [tesser_ranges](https://bitbucket.org/AdrianArtacho/tesser_ranges/)           | Allows/Blocks specific midi ranges dynamically        |
| [tesser_recall](https://bitbucket.org/AdrianArtacho/tesser_recall/)           | Saves and recalls midi sequences dynamically          |
| [tesser_route](https://bitbucket.org/AdrianArtacho/tesser_route/)             | Routes midi input dynamically                         |
| [tesser_scale](https://bitbucket.org/AdrianArtacho/tesser_scale/)             | Scales midi input dynamically                         |
| [tesser_signal2midi](https://bitbucket.org/AdrianArtacho/tesser_signal2midi/) | Takes in a signal (audio) and converts it to midi     |
| [tesser_threshold](https://bitbucket.org/AdrianArtacho/tesser_threshold/)     | Allows/Blocks midi input based on threshold values    |
| [tesser_videoloop](https://bitbucket.org/AdrianArtacho/tesser_videoloop/)     | Live capture looping                                  |
| [tesser_visuals](https://bitbucket.org/AdrianArtacho/tesser_visuals/)         | Produce visuals (Max/jitter) based on midi            |
